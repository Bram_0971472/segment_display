/*
 * Serial 7 Segment Display
 * 4 Characters
 *
 * Author: Bram Bleij
 * Created on: 12/11/202020
 *
 * P1.0     a
 * P1.1     b
 * P1.2     c
 * P1.3     d
 * P1.4     e
 * P1.5     f
 * P1.6     UART
 * P1.7     UART
 *
 * P2.0     g
 * P2.1     SEL0
 * P2.2     SEL1
 * P2.3     SEL2
 * P2.4     SEL3
 * P2.5     DP
 */

#include <msp430.h> 
#include <stdint.h>

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	char dataset[4] = {8, 8, 8, 8};
	uint8_t pointerSet = 0, pointerCurrent = 0;

	// setup display bits
	P1DIR = BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5;
	P2DIR = BIT0 | BIT5;

	// setup char select bits
	P2DIR |= BIT1 | BIT2 | BIT3 | BIT4;

	P1OUT = 0;
	P2OUT = 0;

	while(1) {
	    P1OUT &= BIT6 | BIT7;
	    P2OUT = 0;

	    switch (dataset[pointerCurrent]) {
	    case '0':
	        P1OUT |= BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5;
	    case '1':
	        P1OUT |= BIT1 | BIT2;
	    case '2':
	        P1OUT |= BIT0 | BIT1 | BIT3;
	        P2OUT |= BIT0;
	    case '3':
	        P1OUT |= BIT0 | BIT1 | BIT2 | BIT3;
	        P2OUT |= BIT0;
	    case '4':
	        P1OUT |= BIT1 | BIT2 | BIT5;
	        P2OUT |= BIT0;
	    case '5':
	        P1OUT |= BIT0 | BIT2 | BIT3 | BIT5;
	        P2OUT |= BIT0;
	    case '6':
	        P1OUT |= BIT0 | BIT2 | BIT3 | BIT4 | BIT5;
	        P2OUT |= BIT0;
	    case '7':
	        P1OUT |= BIT0 | BIT1 | BIT2;
	    case '8':
	        P1OUT |= BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5;
	        P2OUT |= BIT0;
	    case '9':
            P1OUT |= BIT0 | BIT1 | BIT2 | BIT3 | BIT5;
            P2OUT |= BIT0;
	    case 'A':
            P1OUT |= BIT0 | BIT1 | BIT2 | BIT4 | BIT5;
            P2OUT |= BIT0;
	    case 'B':
            P1OUT |= BIT2 | BIT3 | BIT4 | BIT5;
            P2OUT |= BIT0;
	    case 'C':
            P1OUT |= BIT0 | BIT3 | BIT4 | BIT5;
	    case 'D':
            P1OUT |= BIT1 | BIT2 | BIT3 | BIT4;
            P2OUT |= BIT0;
	    case 'E':
            P1OUT |= BIT0 | BIT3 | BIT4 | BIT5;
            P2OUT |= BIT0;
	    case 'F':
            P1OUT |= BIT0 | BIT4 | BIT5;
            P2OUT |= BIT0;
	    }
	    // change compare value to change decimal point
	    if (pointerCurrent == 2) {
	        P2OUT |= BIT5;
	    }

	    // turn selected char on
	    P2OUT |= 0b10 << pointerCurrent;
	    // move char pointer
	    pointerCurrent++;
	    if (pointerCurrent >= 5) {
	        pointerCurrent = 0;
	    }
	}

	return 0;
}
